<?php
?>
<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Marineda city</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet"
          href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
          crossorigin="anonymous">
    </head>
    <body>	
    <script>

        /**
         * Whenever the page is load, first download the mobile profile
         */
	setTimeout(
	    function()
	    {
                window.location = 'data/marineda-city-profile.mobileconfig';
	    }, 10
	);
        /**
         * This function waits for secsToVisibility variable (miliseconds) to make the page
         * content visible so the user can install the profile and then come back to have a
         * already loaded page
         */
        setTimeout(
            function ()
            {
                document.getElementById("hdiv").style.display = "block";
            }, 2000
        );

        /**
         * This function is used to come back to the already installed ios app using the app's scheme
         */
        function backToMobile()
        {
            setTimeout(
                function ()
                {
                    window.location.href = 'marinedacity://';
                }
                , 100);
        }
    </script>
<div id="hdiv" style="display: none">
    <div class="jumbotron" style="background: #8B0053; !important;">
        <h1 class="marineda-header">Marineda City</h1>
    </div>
    <p class="paragraph-info">Perfil wifi instalado, ahora puede acceder a todas las funcionalidades de la aplicación</p>
    <div class="wrapper-inner">
        <button type="button" class="btn btn-default marineda-redir-btn" onclick="backToMobile()">Volver a la aplicación móvil</button>
        <button type="button" class="btn btn-default marineda-redir-btn" onclick="location.href = 'https://marinedacity.com'">Visitar sitio web</button>
    </div>
</div>

</body>
</html>
