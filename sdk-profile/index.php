<!--
    Post Params
    All variables are sent via GET
        profile-title: A variable for the window title
        profile-filename: A variable with the filename of the profile created with apple configurator 2
        profile-main-color: The color of the jumbotron
        profile-main-title: Title as shown in the jumbotron
        profile-website: Url with the website of the mall
        profile-schema: The url type schema to redirect to ios app
        profile-font-color: The color of jumbotron font

    Default values:
        profile-title: 'Wifi-profile'
        profile-filename: 'WifiProfile.mobileconfig'
        profile-main-color: '#000000'
        profile-main-title: 'Perfil wifi'
        profile-website: 'Does not shown if do not exists or empty'
        profile-schema: schema?
        profile-font-color: The color of jumbotron font
-->
<!doctype html>
<html class="no-js">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>
        <?php
        if (isset($_GET['profile-title']) && !empty($_GET['profile-title']))
        {
            echo $_GET['profile-title'];
        }
        else
        {
            echo 'Wifi profile';
        }
        ?>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet"
          href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
          crossorigin="anonymous">
</head>
<body>
<script>
    /**
     * Whenever the page is load, first download the mobile profile
     */

    setTimeout(
        function()
        {
            window.location =
            <?php
            if (isset($_GET['profile-filename']) && !empty($_GET['profile-filename']))
            {
                echo "'data/".$_GET['profile-filename'].".mobileconfig'";
            }
            else
            {
                echo "'data/SeeketingDefaultProfileSigned.mobileconfig'";
            }
            ?>;
        }, 10
    );
    /**
     * This function waits for secsToVisibility variable (miliseconds) to make the page
     * content visible so the user can install the profile and then come back to have a
     * already loaded page
     */
    setTimeout(
        function ()
        {
            document.getElementById("hdiv").style.display = "block";
        }, 1
    );

    /**
     * This function is used to come back to the already installed ios app using the app's scheme
     */
    function backToMobile()
    {
        setTimeout(
            function ()
            {
                window.location.href = '<?php
                if (isset($_GET['profile-schema']) && !empty($_GET['profile-schema']))
                {
                    echo $_GET['profile-schema']."://";
                }
                else
                {
                    echo 'schema://';
                }
                ?>';
            }
            , 100);
    }
</script>
<div id="hdiv" style="display: none">
    <div class="jumbotron" style="background:
    <?php
    if (isset($_GET['profile-main-color']) && !empty($_GET['profile-main-color']))
    {
        echo "#".$_GET['profile-main-color'];
    }
    else
    {
        echo '#000000';
    }
    ?>; !important;">
        <h1 class="marineda-header" style="color:
        <?php
        if (isset($_GET['profile-font-color']) && !empty($_GET['profile-font-color']))
        {
            echo "#".$_GET['profile-font-color'];
        }
        else
        {
            echo '#FFFFFF';
        }
        ?> !important">


            <?php
            if (isset($_GET['profile-main-title']) && !empty($_GET['profile-main-title']))
            {
                echo $_GET['profile-main-title'];
            }
            else
            {
                echo 'Perfil wifi';
            }
            ?>


        </h1>
    </div>
    <p class="paragraph-info">Perfil wifi instalado, ahora puede acceder a todas las funcionalidades de la aplicación</p>
    <div class="wrapper-inner">
        <button type="button" class="btn btn-default marineda-redir-btn" onclick="backToMobile()">Volver a la aplicación móvil</button>
        <?php
        if (isset($_GET['profile-website']) && !empty($_GET['profile-website']))
        {
            ?>
            <button type="button" class="btn btn-default marineda-redir-btn" onclick="location.href= '<?php echo("https://".$_GET['profile-website']);?>'">Visitar sitio web</button>
            <?php
        }
        else
        {
            //Do not show btn
        }
        ?>
    </div>
</div>

</body>
</html>
