Perfiles específicos:

- marineda-profile

Perfiles generales:

- sdk-profile

Funcionamiento

    Comenzamos con el div con id = hdiv con style="display: none" para que el contenido de la web sea transparente.
    Lo primero es que una funcion js con un temporizador a 10 milisegundos llama al path local del archivo de configuración,
    esto permite que se descargue y se ejecute en el iPhone
    Otro temporizador cuenta 2 segundos y hace el contenido visible, asi que una el usuario termine de instalar el perfil, puede ir atras
    y la pagina ya estara preparada.

